import React from "react";
import {Col, Row } from "react-bootstrap"
import styles from "../../styles/Header.module.scss"
import Logo from "../../components/logo"
import Link from "next/link";


const Header = () => {
	return(
		<Row className={"d-flex align-items-center py-1"}>
			<Col>
				<Link href={"/"} ><span className={styles.header__link}>Главная</span></Link>
			</Col>
			<Col>
				<Link href={"/constructor"} ><span className={styles.header__link}>Конструктор</span></Link>
			</Col>
			<Col>
				<Link href={"/gallery"}><span className={styles.header__link}>Галерея</span></Link>
			</Col>
			<Col>
				<Link href={"/about"}><span className={styles.header__link}>О нас</span></Link>
			</Col>
			<Col>
				<Link href={"/catalog"}><span className={styles.header__link}>Каталог</span></Link>
			</Col>
			<Col>
				<Logo />
			</Col>
		</Row>
	)
}
export default Header