import React from "react"
import {Button } from "react-bootstrap"

export const OutlinedButton = ({ text }) => {
	return(
		<Button className="btn button-outlined">
			{text}
		</Button>
	)
}