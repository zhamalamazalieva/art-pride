import styles from "../../styles/Home.module.scss"
import {Col, Row } from "react-bootstrap"
import { OutlinedButton } from "../buttons/Buttons";

const HomeColorItem = () => {
	return(
		<Row className="">
			<Col  className={""}>
				<Row className={"flex-column justify-content-center"}>
					<Col className={"d-flex align-item-center justify-content-center mb-3"}>
						<div className={styles.home__item} />
					</Col>
					<Col className={"text-center"}>
						<OutlinedButton text={"Заказ"}/>
					</Col>
					<Col className={"text-center"}>
						<p className={styles.home__price}>700-1200 som</p>
					</Col>
				</Row>
			</Col>
			<Col>
				<h2 className={styles.home__title}>vcxvcxvxcvxcv</h2>
			</Col>
		</Row>
	)
}
export default HomeColorItem