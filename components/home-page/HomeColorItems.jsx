import styles from "../../styles/Home.module.scss"
import {Col, Row } from "react-bootstrap"

const HomeColorItems = () => {
	return(
		<Row className="flex-wrap">
			<Col  className={"col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-4 mb-3"}>
				<div className={"circle"} />
			</Col>
			<Col  className={"col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-4 mb-3"}>
				<div className={styles.home__colorItem} />
			</Col>
			<Col  className={"col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-4 mb-3"}>
				<div className={styles.home__colorItem} />
			</Col>
			<Col className={"col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-4 mb-3"} >
				<div className={styles.home__colorItem} />
			</Col>
			<Col  className={"col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-4 mb-3"}>
				<div className={"circle"} />
			</Col>
			<Col  className={"col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-4 mb-3"}>
				<div className={styles.home__colorItem} />
			</Col>
			<Col  className={"col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-4 mb-3"}>
				<div className={styles.home__colorItem} />
			</Col>
			<Col className={"col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-4 mb-3"} >
				<div className={styles.home__colorItem} />
			</Col>
			<Col  className={"col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-4 mb-3"}>
				<div className={styles.home__colorItem} />
			</Col>
			<Col className={"col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-4 mb-3"} >
				<div className={styles.home__colorItem} />
			</Col>
		</Row>
	)
}
export default HomeColorItems