import {Col, Row } from "react-bootstrap"

export default function Custom404() {
	return(
		<div style={{background:"linear-gradient(140.55deg, #000000 26.78%, #2F2F2F 75.39%)", opacity:"0.8"}}>
			<Row className="container align-items-center justify-content-center">
				<Col className={"error-page-wrapper"}>
					<h1 className="error-page">404 - Page Not Found</h1>
				</Col>
			</Row>
		</div>
	)
}