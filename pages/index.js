import MainContainer from "../components/main-container/MainContainer";
import styles from "../styles/Home.module.scss"
import {Row, Col } from "react-bootstrap"
import HomeColorItems from "../components/home-page/HomeColorItems";
import HomeColorItem from "../components/home-page/HomeColorItem";

export default function Home() {
  return (
      <MainContainer bgTheme={{background:"linear-gradient(140.55deg, #000000 26.78%, #2F2F2F 75.39%)", opacity:"0.8"}}>
        <Row className={"pt-4"}>
          <Col className={"col-12 col-xxl-9 col-xl-9 col-lg-9 col-md-9"}>
            <HomeColorItem/>
          </Col>
          <Col className={"col-12 col-xxl-3 col-xl-3 col-lg-23 col-md-3"}>
            <HomeColorItems/>
          </Col>
        </Row>
      </MainContainer>
  )
}
